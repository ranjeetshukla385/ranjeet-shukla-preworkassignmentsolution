import java.util.Scanner;

public class Prework {

	Scanner sc = new Scanner(System.in);
	int num = 0;

	// function to checkPalindrome
	public void checkPalindrome() {
		System.out.println("Enter number to check the number palindrom or not -");
		num = sc.nextInt();
		int r, sum = 0;
		int temp = num;
		while (num > 0) {
			r = num % 10;
			sum = (sum * 10) + r;
			num = num / 10;
		}
		if (temp == sum)
			System.out.println(temp + " is palindrome number.");
		else
			System.out.println(temp + " is not a palindrome number.");

	}

	// function to printPattern
	public void printPattern() {
		System.out.println("Enter number to print the pattern of stars -");
		num = sc.nextInt();
		for (int i = num; i > 0; i--) {
			for (int j = i; j > 0; j--) {
				System.out.print("*");
			}
			System.out.println("");

		}

	}

	// function to check no is prime or not
	public void checkPrimeNumber() {
		System.out.println("Enter number");
		num = sc.nextInt();
		boolean isPrime = true;
		if (num == 0 || num == 1) {
			System.out.println(num + " is not prime number");
		} else {
			for (int i = 2; i < num / 2; i++) {
				if (num % i == 0) {
					System.out.println(num + " is not prime number");
					isPrime = false;
					break;
				}

			}
			if (isPrime) {
				System.out.println(num + " is prime number");
			}
		}
	}

	// function to print Fibonacci Series
	public void printFibonacciSeries() {
		System.out.println("Enter number to print fibonacci series");
		num = sc.nextInt();
		// initialize the first and second value as 0,1 respectively.
		int first = 0, second = 1;
		for (int i = 1; i <= num; i++) {
			System.out.print(first + ", ");

			int nextItem = first + second;
			first = second;
			second = nextItem;
		}

	}

	public static void main(String[] args) {
		Prework obj = new Prework();

		int choice;

		Scanner sc = new Scanner(System.in);

		do {
			System.out.println();
			System.out.println("-------------------");
			System.out.println("Enter your choice from below list.\n" + "1. Find palindrome of number.\n"

					+ "2. Print Pattern for a given no.\n" + "3. Check whether the no is a  prime number.\n"

					+ "4. Print Fibonacci series.\n" + "--> Enter 0 to Exit.\n");

			System.out.println();

			choice = sc.nextInt();

			switch (choice) {

			case 0:

				choice = 0;

				break;

			case 1: {

				obj.checkPalindrome();

			}

				break;

			case 2: {

				obj.printPattern();

			}

				break;

			case 3: {

				obj.checkPrimeNumber();

			}

				break;

			case 4: {

				obj.printFibonacciSeries();

			}

				break;

			default:

				System.out.println("Invalid choice. Enter a valid no.\n");

			}

		} while (choice != 0);

		System.out.println("Exited Successfully!!!");

		sc.close();

	}

}
